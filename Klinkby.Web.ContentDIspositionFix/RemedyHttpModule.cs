﻿using System;
using System.Text.RegularExpressions;
using System.Web;

namespace Klinkby.Web.ContentDispositionFix
{
    public sealed class RemedyHttpModule : IHttpModule
    {
        const string ContentDisposition = "Content-Disposition";
        readonly Regex m_regex = new Regex(
            "(^|[;\\s])filename=(?'fn'[^\";]+)(;|$)",
            RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.ExplicitCapture);

        public void Init(HttpApplication context)
        {
            context.PreSendRequestHeaders += context_PreSendRequestHeaders;
        }

        void context_PreSendRequestHeaders(object sender, EventArgs e)
        {
            var app = (HttpApplication)sender;
            var headers = app.Response.Headers;
            string value = headers[ContentDisposition];
            if (value != null)
            {
                var match = m_regex.Match(value);
                if (match.Success)
                {
                    var fn = match.Groups["fn"];
                    if (fn.Success && fn.Value.IndexOfAny(new [] {',', ' '}) >= 0)
                    {
                        string newValue = value.Substring(0, fn.Index)
                            + "\"" + fn.Value + "\""
                            + value.Substring(fn.Index + fn.Length);
                        headers[ContentDisposition] = newValue;
                    }
                }
            }
        }

        public void Dispose()
        {
        }
    }
}
