﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Klinkby.Web.ContentDispositionFix")]
[assembly: AssemblyDescription("Http module that fixed the ERR_RESPONSE_HEADERS_MULTIPLE_CONTENT_DISPOSITION error in IIS.")]
#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif
[assembly: AssemblyCompany("Mads Klinkby (m@kli.dk)")]
[assembly: AssemblyProduct("Klinkby.Web.ContentDispositionFix")]
[assembly: AssemblyCopyright("Some rights reserved")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: AllowPartiallyTrustedCallers]
[assembly: ComVisibleAttribute(false)]
[assembly: CLSCompliant(true)]

